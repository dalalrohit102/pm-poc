import React, { useState } from "react";
import {
  Button,
  Card,
  CardActionArea,
  CardMedia,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { motion } from "framer-motion";

const useStyles = makeStyles({
  card: {
    position: "relative",
    boxSizing: "border-box",
    border: "1px solid red",
    cursor: "pointer",
    width: "168px",
    height: "223px",
    margin: 0,
    padding: 0,
    "&:hover": {
      $content: {
        opacity: 1,
      },
    },
  },
  thumbnail: {
    width: "100%",
    height: "100%",
  },
  poster: {
    width: "100%",
    height: "100%",
  },
  content: {
    boxSizing: "border-box",
    margin: 0,
    padding: 0,
    width: "100%",
    height: "90px",
    maxHeight: "90px",
    position: "absolute",
    left: 0,
    bottom: 0,
    color: "white",
    // opacity: 0,
    border: "1px solid white",
  },
  show: {
    transition: "all .1s ease-in",
    opacity: 1,
  },
});
export default function MyCard() {
  const [hover, setHover] = useState<boolean>(false);
  const classes = useStyles();
  return (
    <div
      onMouseOver={() => setHover(true)}
      onMouseOut={() => setHover(false)}
      className={classes.card}
    >
      {/* poster */}
      <div className={classes.thumbnail}>
        <img
          className={classes.poster}
          alt="Picture of the author"
          src="https://images.unsplash.com/photo-1489549132488-d00b7eee80f1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
        />
      </div>

      <div
        // className={
        //   hover ? `${classes.content} ${classes.show}` : classes.content
        // }
        className={classes.content}
      >
        <h2>okay</h2>
      </div>
    </div>
  );
}
