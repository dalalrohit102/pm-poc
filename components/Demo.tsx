import React from "react";
import { Replay } from "vimond-replay";
import "vimond-replay/index.css";
import CompoundVideoStreamer from "vimond-replay/video-streamer/compound";
export default function Demo() {
  return (
    <Replay
      source="https://videodelivery.net/71ea1ace1013f314e65454c194a87ee7/manifest/video.mpd"
      initialPlaybackProps={{ isPaused: true }}
    >
      <CompoundVideoStreamer />
    </Replay>
  );
}
