import React, { forwardRef } from "react";
import classes from "./../styles/player.module.scss";
import {
  Grid,
  Typography,
  IconButton,
  Slider,
  Tooltip,
  Zoom,
  CircularProgress,
} from "@material-ui/core";
import { ControlProps, ValueLabelProps } from "./../interfaces";
import { makeStyles, Theme, withStyles } from "@material-ui/core/styles";
//icons
import FastRewindIcon from "@material-ui/icons/FastRewind";
import FastForwardIcon from "@material-ui/icons/FastForward";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import VolumeOffIcon from "@material-ui/icons/VolumeOff";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import FullscreenExitIcon from "@material-ui/icons/FullscreenExit";
import SettingsIcon from "@material-ui/icons/Settings";

const PrettoSlider = withStyles({
  root: {
    boxSizing: "border-box",
    color: "#ff22bd",
    height: 5,
    padding: 0,
  },
  thumb: {
    height: 15,
    width: 15,
    backgroundColor: "#fff",
    border: "1px solid currentColor",
    marginTop: -5,
    marginLeft: 0,
    transition: "all .2s ease-in",
    "&:focus, &:hover, &$active": {
      boxShadow: "inherit",
      transform: "scale(1.5)",
    },
  },
  valueLabel: {
    left: "calc(-50% + 4px)",
  },
  track: {
    height: 5,
    borderRadius: 2.5,
  },
  rail: {
    height: 5,
    borderRadius: 2.5,
  },
})(Slider);

function ValueLabelComponent(props: ValueLabelProps) {
  const { children, open, value } = props;

  return (
    <Tooltip
      arrow
      TransitionComponent={Zoom}
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={value}
    >
      {children}
    </Tooltip>
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  button: {
    opacity: 0.68,
    "& $span": {
      "& $svg": {
        fill: "#eee",
      },
    },
    "&:hover": {
      color: "white",
      transform: "scale(1.25)",
      opacity: 1,
    },
  },
  ctrlBtn: {
    color: "#efefef",
    opacity: 0.8,
    padding: 8,
    "&:hover": {
      opacity: 1,
      color: "white",
      transform: "scale(1.25)",
    },
  },
  wrapper: {
    boxSizing: "border-box",
    display: "flex",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  spinner: {
    color: "hotpink",
  },
  bottomWrapper: {
    boxSizing: "border-box",
    border: "1px solid black",
    padding: "0 8px 0 8px",
    height: "auto",
    width: "100%",

    [theme.breakpoints.down("xs")]: {
      padding: 0,
    },
  },
  seekWrapper: {
    boxSizing: "border-box",
    border: "1px solid pink",
    padding: 0,
  },
  toolsWrapper: {
    border: "1px solid violet",
  },
  timer: {
    boxSizing: "border-box",
    border: "1px solid darkblue",
    textAlign: "center",
    padding: 4,
  },
}));

// Main export
const MyControls = forwardRef(
  (props: ControlProps, ref: React.Ref<HTMLDivElement>) => {
    const {
      state,
      elapsedTime,
      totalDuration,
      onPlayPause,
      onMuteUnmute,
      onVolumeHover,
      onVolumeHoverOut,
      onToggleFullScreen,
      onSeekMouseUp,
      onSeekMouseDown,
      handleVolumeSeekDown,

      handleForward,
      handleRewind,
      handleSeekChange,
      handleVolume,
    } = props;
    const {
      fullscreen,
      playing,
      mute,
      played,
      buffering,
      volume,
      volumeBar,
    } = state;
    const muiclasses = useStyles();
    return (
      <div ref={ref} className={classes.controlsWrapper}>
        {/* ==================TOP- title,logo,meta */}
        <Grid
          className={classes.top}
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <Grid item>
            <Typography>Sample</Typography>
          </Grid>

          <Grid item>
            <Typography variant="body1" color="inherit">
              planet marathi
            </Typography>
          </Grid>
        </Grid>

        {/* ==================MIDDLE - center controls */}
        <Grid
          container
          className={classes.middle}
          onClick={onPlayPause}
          direction="row"
          alignItems="center"
          justify="center"
        >
          {buffering ? (
            <CircularProgress
              className={muiclasses.spinner}
              color="inherit"
              size={80}
            />
          ) : (
            <Grid item>
              <IconButton onClick={onPlayPause} className={muiclasses.button}>
                {playing ? (
                  <PauseIcon fontSize="large" />
                ) : (
                  <PlayArrowIcon fontSize="large" />
                )}
              </IconButton>
            </Grid>
          )}
        </Grid>

        {/* ==================BOTTOM - options */}
        <Grid
          className={muiclasses.bottomWrapper}
          container
          justify="space-between"
          alignItems="center"
        >
          {/* seeker, elapsed, total duration */}
          <Grid item xs={12}>
            <Grid container>
              <Grid className={muiclasses.timer} item xs={1}>
                {elapsedTime}
              </Grid>
              <Grid className={muiclasses.seekWrapper} item xs={10}>
                <PrettoSlider
                  min={0}
                  max={100}
                  value={played * 100}
                  onMouseDown={onSeekMouseDown}
                  onChange={handleSeekChange}
                  onChangeCommitted={onSeekMouseUp}
                  valueLabelFormat="string"
                  valueLabelDisplay="auto"
                  ValueLabelComponent={(props) => (
                    <ValueLabelComponent {...props} value={elapsedTime} />
                  )}
                />
              </Grid>
              <Grid className={muiclasses.timer} item xs={1}>
                {totalDuration}
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid container>
              <Grid className={muiclasses.toolsWrapper} item xs={8}>
                <Grid container direction="row" alignItems="center">
                  {/* PLAY/PAUSE BUTTON */}
                  <IconButton
                    className={muiclasses.ctrlBtn}
                    onClick={onPlayPause}
                  >
                    {playing ? <PauseIcon /> : <PlayArrowIcon />}
                  </IconButton>
                  {/* REWIND BUTTON */}
                  <IconButton
                    onClick={handleRewind}
                    className={muiclasses.ctrlBtn}
                  >
                    <FastRewindIcon />
                  </IconButton>

                  {/* FORWARD BUTTON */}
                  <IconButton
                    onClick={handleForward}
                    className={muiclasses.ctrlBtn}
                  >
                    <FastForwardIcon />
                  </IconButton>
                </Grid>
              </Grid>
              <Grid className={muiclasses.toolsWrapper} item xs={4}>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="flex-end"
                >
                  <IconButton className={muiclasses.ctrlBtn}>
                    <SettingsIcon />
                  </IconButton>
                  <IconButton
                    className={muiclasses.ctrlBtn}
                    onClick={onToggleFullScreen}
                  >
                    {fullscreen ? <FullscreenExitIcon /> : <FullscreenIcon />}
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
);

export default MyControls;
