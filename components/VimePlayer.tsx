import React, { useState, useRef, useEffect } from "react";
import {
  Controls,
  Player,
  Dash,
  Ui,
  PlaybackControl,
  ClickToPlay,
  FullscreenControl,
  VolumeControl,
  ScrubberControl,
  Spinner,
  DefaultUi,
  Scrim,
  CurrentTime,
  EndTime,
  Time,
  Control,
  LoadingScreen,
} from "@vime/react";
import { Grid, Typography, makeStyles } from "@material-ui/core";
import TapToSeek from "./../components/TapToSeek";

const useStyles = makeStyles({
  container: {
    border: "1px solid red",
  },
});
export default function VimePlayer() {
  const ref = useRef<HTMLVmPlayerElement>(null);
  const classes = useStyles();

  const onFullscreenChange = (event: CustomEvent<boolean>) => {
    const isFullscreen = event.detail;
    console.log(ref, isFullscreen);
  };
  return (
    <div
      style={{
        width: 800,
        height: 400,
      }}
    >
      <Player
        ref={ref}
        onVmFullscreenChange={onFullscreenChange}
        theme="dark"
        style={{ "--vm-player-theme": "#e86c8b" }}
      >
        <Dash
          src="https://videodelivery.net/71ea1ace1013f314e65454c194a87ee7/manifest/video.mpd"
          version="latest"
        />
        <Ui>
          <Scrim gradient="up" />

          <DefaultUi>
            <TapToSeek />
          </DefaultUi>
        </Ui>
        {/* <Ui>
          <Scrim gradient="up" />

          <Controls fullWidth justify="space-between" pin="topRight">
            <Typography style={{ color: "white" }}>Title</Typography>
            <Typography style={{ color: "white" }}>Title</Typography>
          </Controls>

          <Controls fullWidth justify="center" pin="center">
            <LoadingScreen />
            <Spinner />
            <PlaybackControl
              hideTooltip
              style={{ "--vm-control-scale": 1.5 }}
            />
          </Controls>

          <Controls
            direction="column"
            fullWidth
            justify="space-between"
            pin="bottomRight"
          >
            <Controls fullWidth>
              <Typography>
                <CurrentTime />
              </Typography>
              <ScrubberControl style={{ margin: "0px 16px 0px 16px" }} />
              <Typography>
                <EndTime />
              </Typography>
            </Controls>

            <Controls fullWidth>
              <Typography>asdjkhlkjh</Typography>
            </Controls>
          </Controls>
        </Ui> */}
      </Player>
    </div>
  );
}
