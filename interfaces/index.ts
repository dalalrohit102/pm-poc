import React from "react";

export type User = {
  id: number;
  name: string;
};

export type ValueLabelProps = {
  children: React.ReactElement;
  open: boolean;
  value: string;
};

export type PlayerStateProps = {
  mute: boolean;
  playing: boolean;
  played: number;
  duration: number;
  volume: number;
  seeking: boolean;
  buffering: boolean;
  volumeBar: boolean;
  fullscreen: boolean;
};

export type ControlProps = {
  state: PlayerStateProps;
  elapsedTime: string;
  totalDuration: string;

  onPlayPause: () => void;
  onMuteUnmute: () => void;
  onVolumeHover: () => void;
  onVolumeHoverOut: () => void;
  onToggleFullScreen: () => void;
  onSeekMouseDown: () => void;
  onSeekMouseUp: (event: any, newValue: number | number[]) => void;
  handleRewind: () => void;
  handleForward: () => void;
  handleSeekChange: (event: any, newValue: number | number[]) => void;
  handleVolume: (event: any, newValue: number | number[]) => void;
  handleVolumeSeekDown: (event: any, newValue: number | number[]) => void;
};
