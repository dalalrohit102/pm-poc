import React from "react";
import { makeStyles } from "@material-ui/core";
import { motion } from "framer-motion";
import Card from "./../components/Card";

const useStyles = makeStyles({
  container: {
    boxSizing: "border-box",
    width: "100%",
    height: "100%",
    backgroundColor: "lightblue",
    display: "flex",
    flexFlow: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    boxSizing: "border-box",
    width: "100%",
    maxWidth: "250px",
    height: "auto",
    margin: "0 .5rem 0 .5rem",
  },

  lolomo: {
    boxSizing: "border-box",
    border: "1px solid red",
    width: "100%",
    height: "auto",
    overflowX: "auto",
    display: "flex",
  },
});

export default function MyCard() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <ul className={classes.lolomo}>
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((k, i) => {
          const left = i === 0; //check first index
          const right = i === 9; //check last index
          return (
            <motion.li
              className={classes.root}
              whileHover={{
                scale: 1.25,
                translateX: left ? 35 : right ? -35 : 0,
                zIndex: 10,
              }}
              key={k}
            >
              <Card />
            </motion.li>
          );
        })}
      </ul>
    </div>
  );
}
