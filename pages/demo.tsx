import { Container } from "@material-ui/core";
import React, { useRef } from "react";
import ReactPlayer from "react-player";
import classes from "./../styles/player.module.scss";
import MyControls from "./../components/MyControls";
import * as screenfull from "screenfull";
import { Screenfull } from "screenfull";
import { PlayerStateProps } from "./../interfaces";
import { format } from "./../utils/utils";
let count = 0;

export default function Demo() {
  //refs
  const playerRef = useRef<ReactPlayer>(null);
  const playerContainerRef = useRef<HTMLDivElement>(null);
  const controlsRef = useRef<HTMLDivElement>(null);

  //state
  const [state, setState] = React.useState<PlayerStateProps>({
    playing: false,
    mute: false,
    played: 0,
    duration: 0,
    volume: 1,
    seeking: false,
    buffering: false,
    volumeBar: false,
    fullscreen: false,
  });

  //handlers
  //1. Volume
  const handleVolume = (_event: any, newVolume: number | number[]) => {
    setState({
      ...state,
      volume: (newVolume as number) / 100,
      mute: newVolume === 0 ? true : false,
    });
  };
  const onMuteUnmute = () => {
    setState({ ...state, mute: !state.mute });
  };

  const onVolumeHover = () => {
    setState({ ...state, volumeBar: true });
  };

  const onVolumeHoverOut = () => {
    setState({ ...state, volumeBar: false });
  };

  const handleVolumeSeekDown = (_event: any, newValue: number | number[]) => {
    setState({ ...state, seeking: false, volume: (newValue as number) / 100 });
  };

  //2. Play, >>, <<
  const onPlayPause = () => {
    console.log("on play pause");
    setState({ ...state, playing: !state.playing });
  };

  const handleRewind = () => {
    if (playerRef.current) {
      playerRef.current.seekTo(playerRef.current.getCurrentTime() - 10);
    }
  };

  const handleForward = () => {
    if (playerRef.current) {
      playerRef.current.seekTo(playerRef.current.getCurrentTime() + 10);
    }
  };

  //3. Full-Screen
  const onToggleFullScreen = () => {
    let sf = screenfull as Screenfull;
    if (sf.isEnabled && playerContainerRef.current) {
      sf.on("change", () => {
        setState({ ...state, fullscreen: sf.isFullscreen });
      });
      sf.toggle(playerContainerRef.current);
    }
  };

  //4. Slider (Seeker)
  const handleProgress = (changeState: any) => {
    // if (count >= 1) {
    //   controlsRef.current!.style.visibility = "hidden";
    //   count = 0;
    // }

    // if (controlsRef.current?.style.visibility === "visible") {
    //   count += 1;
    // }

    if (!state.seeking) {
      setState({
        ...state,
        ...changeState,
      });
    }
  };
  const handleSeekChange = (_event: any, newValue: number | number[]) => {
    setState({
      ...state,
      played: (newValue as number) / 100,
    });
  };
  const onSeekMouseDown = () => {
    setState({ ...state, seeking: true });
  };
  const onSeekMouseUp = (_event: any, newValue: number | number[]) => {
    setState({ ...state, seeking: false });
    playerRef.current?.seekTo((newValue as number) / 100);
  };

  const handleMouseMove = () => {
    if (controlsRef.current) {
      controlsRef.current.style.visibility = "visible";
      count = 0;
    }
  };

  const currentTime = playerRef.current
    ? playerRef.current.getCurrentTime()
    : "00:00";
  const duration = playerRef.current
    ? playerRef.current.getDuration()
    : "00:00";

  const elapsedTime = format(currentTime);
  const totalDuration = format(duration);

  const { volume, mute, playing, played, buffering } = state;

  const videoUrl =
    "https://videodelivery.net/71ea1ace1013f314e65454c194a87ee7/manifest/video.mpd";
  // JSX
  return (
    <Container maxWidth="lg" disableGutters>
      <div
        ref={playerContainerRef}
        className={classes.playerWrapper}
        onMouseMove={handleMouseMove}
      >
        <ReactPlayer
          ref={playerRef}
          url={videoUrl}
          width="100%"
          height="100%"
          muted={mute}
          playing={playing}
          volume={volume}
          onProgress={handleProgress}
          loop={false}
          onEnded={onPlayPause}
          onBuffer={() => {
            console.log("buffer start");
            setState({
              ...state,
              buffering: true,
            });
          }}
          onBufferEnd={() => {
            console.log("buffer end");

            setState({
              ...state,
              buffering: false,
            });
          }}
          config={{
            file: {
              forceVideo: true,
              forceDASH: true,
            },
          }}
        />

        <MyControls
          //ref
          ref={controlsRef}
          //vars
          state={state}
          elapsedTime={elapsedTime}
          totalDuration={totalDuration}
          onPlayPause={onPlayPause}
          //volume
          onMuteUnmute={onMuteUnmute}
          onVolumeHover={onVolumeHover}
          onVolumeHoverOut={onVolumeHoverOut}
          handleVolume={handleVolume}
          handleVolumeSeekDown={handleVolumeSeekDown}
          //full screen
          onToggleFullScreen={onToggleFullScreen}
          //seeker
          onSeekMouseUp={onSeekMouseUp}
          onSeekMouseDown={onSeekMouseDown}
          handleRewind={handleRewind}
          handleForward={handleForward}
          handleSeekChange={handleSeekChange}
        />
      </div>
    </Container>
  );
}
