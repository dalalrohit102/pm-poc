import React from "react";

import dynamic from "next/dynamic";

const MyPlayer = dynamic(() => import("./../components/Demo"), {
  ssr: false,
});
export default function replay() {
  return <MyPlayer />;
}
