import React from "react";
import dynamic from "next/dynamic";
const MyPlayer = dynamic(() => import("./../components/VimePlayer"), {
  ssr: false,
});
export default function vime() {
  return (
    <div style={{ display: "flex" }}>
      <MyPlayer />
    </div>
  );
}
